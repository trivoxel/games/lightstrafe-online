# LightStrafe - Online
## Project currently on hold indefinitely.
_**Reasons: Game Engine needs to mature, game design must be reworked and brought back to the game's roots.**_


LightStrafe - Online is a fun online platformer video game with shooting mechanics.

[Visit the Website](https://playlso.page.link/site)

[Play the Game](https://playlso.page.link/gg)

![Image](https://lightstrafe.weebly.com/uploads/1/2/1/4/121482671/published/lightstrafe-online.png?1538094564)

Special thanks to our friends at [Reborn OS](https://reborn-os.weebly.com)!